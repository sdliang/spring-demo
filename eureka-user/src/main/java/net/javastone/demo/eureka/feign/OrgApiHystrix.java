package net.javastone.demo.eureka.feign;

import net.javastone.demo.eureka.Org;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.UUID;

/**
 * Created by user-007 on 2017/11/9.
 */
@Component
public class OrgApiHystrix implements OrgApi {

    @Override
    public Org hiOrg(@PathVariable(value = "name") String name) {
        Org org = new Org();
        org.setId(UUID.randomUUID().toString());
        org.setName("server is error:" + name);
        return org;
    }
}
