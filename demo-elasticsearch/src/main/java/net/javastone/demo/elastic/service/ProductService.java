package net.javastone.demo.elastic.service;

import net.javastone.demo.elastic.metadata.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by sdliang on 2019/5/31.
 */
public interface ProductService {

  /**
   * 保存
   *
   * @param product
   * @return
   */
  String save(Product product);

  /**
   * 查询
   *
   * @param id
   * @return
   */
  Product get(String id);

  /**
   * 更新(先删除,再增加)
   *
   * @param product
   */
  void update(Product product);

  /**
   * 删除
   *
   * @param id
   */
  void deleteById(String id);

  /**
   * 查询
   *
   * @param keyword  关键字
   * @param pageable 分页(从0开始)
   * @return
   */
  Page<Product> findByKeyword(String keyword, Pageable pageable);
}
