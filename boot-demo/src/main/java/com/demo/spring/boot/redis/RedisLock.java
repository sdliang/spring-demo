package com.demo.spring.boot.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisOperations;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by user-007 on 2017/11/23.
 */
public class RedisLock {

    private Logger logger = LoggerFactory.getLogger(getClass());
    /**
     * 获取锁, 循环重试时间(毫秒)
     */
    private static final int DEFAULT_ACQUIRY_RETRY_MILLIS = 100;
    /**
     * 锁后缀
     */
    private static final String LOCK_SUFFIX = "_redis_lock";
    /**
     * 默认失效时间(毫秒)
     */
    private long expireMillis = 60 * 1000;

    private RedisOperations operations;
    /**
     * 锁唯一标识
     */
    private String lockKey;

    private volatile boolean locked = false;

    public String getLockKey() {
        return lockKey;
    }

    public RedisLock(RedisOperations operations) {
        this.operations = operations;
        this.lockKey = UUID.randomUUID().toString() + LOCK_SUFFIX;
    }

    public RedisLock(RedisOperations operations, String lockKey) {
        this.operations = operations;
        this.lockKey = lockKey + LOCK_SUFFIX;
    }

    public RedisLock(RedisOperations operations, int expireMillis) {
        this.expireMillis = expireMillis;
        this.operations = operations;
        this.lockKey = UUID.randomUUID().toString() + LOCK_SUFFIX;
    }

    public RedisLock(RedisOperations operations, String lockKey, long expireMillis) {
        this.expireMillis = expireMillis;
        this.operations = operations;
        this.lockKey = lockKey + LOCK_SUFFIX;
    }

    private String get(String key) {
        Object obj = operations.opsForValue().get(key);
        return obj == null ? null : obj.toString();
    }

    private boolean setIfAbsent(String key, String value) {
        return operations.opsForValue().setIfAbsent(key, value);
    }

    private String getAndSet(String key, String value) {
        Object obj = operations.opsForValue().getAndSet(key, value);
        return obj == null ? null : obj.toString();
    }

    private void delete(String key) {
        operations.delete(key);
    }

    /**
     * 尝试加锁
     *
     * @return
     */
    public boolean tryLock() {
        return innerLock(0L, TimeUnit.NANOSECONDS);
    }

    /**
     * 尝试加锁
     *
     * @param timeout 等待超时
     * @param unit    时间单位
     * @return
     */
    public boolean tryLock(long timeout, TimeUnit unit) {
        return innerLock(timeout, unit);
    }

    /**
     * 加锁
     */
    public void lock() {
        innerLock(-1L, TimeUnit.NANOSECONDS);
    }

    /**
     * 释放
     */
    public void unlock() {
        if (locked) {
            delete(lockKey);
            locked = false;
        }
    }

    /**
     * 尝试加锁
     *
     * @param timeout 等待超时 (0 不等待;-1 永不超时)
     * @param unit    时间单位
     * @return
     */
    private boolean innerLock(long timeout, TimeUnit unit) {
        try {
            long nano = System.nanoTime();

            if (logger.isDebugEnabled()) {
                logger.debug("try lock, key:" + lockKey);
            }
            do {
                long expires = System.currentTimeMillis() + expireMillis + 1;
                String expiresStr = String.valueOf(expires); //锁到期时间

                //先尝试设置key
                if (setIfAbsent(lockKey, expiresStr)) {
                    locked = true;
                    logger.debug("get lock, key: " + lockKey + " , expire in " + expireMillis + " milliseconds.");
                    return true;
                } else {
                    //判断锁是否已经过期，过期则重新设置并获取
                    String currentValue = get(lockKey);
                    if (currentValue != null && Long.parseLong(currentValue) < System.currentTimeMillis()) {
                        //设置锁并返回旧值
                        String oldValue = getAndSet(lockKey, expiresStr);
                        //CAS操作 比较锁的时间，如果不一致则可能是其他锁已经修改了值并获取
                        if (oldValue != null && oldValue.equals(currentValue)) {
                            locked = true;
                            return true;
                        }
                    }
                    if (logger.isDebugEnabled()) {
                        logger.debug("key: " + lockKey + " locked by another business");
                    }
                }
                if (timeout == 0) {
                    break;
                }
                Thread.sleep(DEFAULT_ACQUIRY_RETRY_MILLIS);
            } while (timeout == -1 || (System.nanoTime() - nano) < unit.toNanos(timeout));
        } catch (InterruptedException e) {
            logger.error("get lock error:" + e.getMessage(), e);
        }
        return false;
    }

}
