package net.javastone.demo.elastic.service;

import net.javastone.demo.elastic.ESDemoConstants;
import net.javastone.demo.test.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Ceated by sdliang on 2019/5/31.
 */
public class IndexServiceTest extends BaseTest {

  @Autowired
  private IndexService indexService;

  @Test
  public void testSave() {

    indexService.create(ESDemoConstants.INDEX_PRODUCTS, ESDemoConstants.INDEX);
    boolean exists = indexService.indexExists(ESDemoConstants.INDEX);

    Assert.assertTrue("创建成功", exists);
  }

  @Test
  public void testDelete() {
    indexService.deleteAlias(ESDemoConstants.INDEX_PRODUCTS, ESDemoConstants.INDEX);
    indexService.delete(ESDemoConstants.INDEX_PRODUCTS);
    boolean exists = indexService.indexExists(ESDemoConstants.INDEX);
    Assert.assertFalse("删除失败", exists);
  }

  @Test
  public void testChangeAlias() {
    indexService.deleteAlias(ESDemoConstants.INDEX_PRODUCTS, ESDemoConstants.INDEX);
    boolean exists = indexService.indexExists(ESDemoConstants.INDEX);
    Assert.assertFalse("修改失败: 删除失败", exists);

    String indexName = "products_v2";
    indexService.create(indexName);
    indexService.addAlias(indexName, ESDemoConstants.INDEX);
    exists = indexService.indexExists(ESDemoConstants.INDEX);
    Assert.assertTrue("修改失败: 增加失败", exists);
  }
}
