package com.demo.spring.boot;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import java.net.URL;

/**
 * Created by BlueDream on 2017/11/6.
 */
public class ControllerTestBase extends TestBase {

  @LocalServerPort
  private int port;

  protected URL base;

  protected TestRestTemplate template;

  @Autowired
  public void setTemplate(TestRestTemplate template) {
    this.template = template;
  }

  @Before
  public void setUp() throws Exception {
    this.base = new URL("http://localhost:" + port + "/springboot/");
  }
}
