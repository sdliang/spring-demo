package net.javastone.demo.eureka;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Created by sdliang on 2018/3/26.
 */
@SpringBootApplication
@EnableEurekaServer
public class ServerApp {

  public static void main(String[] args) {
    new SpringApplicationBuilder(ServerApp.class).build().run(args);
  }

}
