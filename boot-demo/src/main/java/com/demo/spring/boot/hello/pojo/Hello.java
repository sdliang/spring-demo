package com.demo.spring.boot.hello.pojo;

import com.demo.spring.boot.pojo.StringPojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by BlueDream on 2017/4/30.
 */
@Entity
@Table(name = "t_face_member")
public class Hello extends StringPojo{

    private String name;

    @Column(name = "gender")
    private Boolean male;

    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getMale() {
        return male;
    }

    public void setMale(Boolean male) {
        this.male = male;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
