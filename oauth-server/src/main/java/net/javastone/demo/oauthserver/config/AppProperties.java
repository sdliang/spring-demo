package net.javastone.demo.oauthserver.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by sdliang on 2018/10/31.
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "app")
public class AppProperties {

  /**
   * 授权模式
   */
  private String[] grantTypes;
  /**
   * token有效时间
   */
  private Integer  tokenValidSeconds;
  /**
   * 客户端应用ID
   */
  private String   clientId;
  /**
   * 客户端应用密钥
   */
  private String   clientSecret;
  /**
   * 授权回调
   */
  private String   clientCallback;
  /**
   * 授权范围
   */
  private String[] scopes;
  /**
   * 白名单
   */
  private String[] oauthBlankUris;

}
