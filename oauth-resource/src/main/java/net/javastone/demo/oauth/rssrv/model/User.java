package net.javastone.demo.oauth.rssrv.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by sdliang on 2018/10/31.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {

  private String id;
  private String name;
  private Integer age;
}
