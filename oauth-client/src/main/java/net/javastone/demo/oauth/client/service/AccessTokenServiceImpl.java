package net.javastone.demo.oauth.client.service;

import net.javastone.demo.oauth.client.config.AppProperties;
import net.javastone.demo.oauth.client.model.AccessToken;
import org.apache.tomcat.util.buf.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AccessTokenServiceImpl implements AccessTokenService {

  private volatile static AccessToken accessToken;

  @Autowired
  private RestTemplate restTemplate;

  @Autowired
  private AppProperties appProperties;

  @Override
  public AccessToken getAccessToken(String code) {
    if (accessToken == null || !accessToken.isValid()) {
      //get remote access_token
      accessToken = call(code);
    }
    return accessToken;
  }

  private AccessToken call(String code) {
    //发送请求
    ResponseEntity<AccessToken> responseEntity = restTemplate.exchange(URI.create(appProperties.getAccessTokenUri()),
                                                                       HttpMethod.POST, getRequestBody(code),
                                                                       AccessToken.class);
    //结果处理
    if (responseEntity.getStatusCode() != HttpStatus.OK) {
      throw new RuntimeException("获取AccessToken失败: " + responseEntity.getStatusCodeValue());
    }
    return responseEntity.getBody();
  }

  /**
   * 获取请求体
   *
   * @param code
   * @return
   */
  private HttpEntity getRequestBody(String code) {
    //header
    HttpHeaders headers = new HttpHeaders();
    headers.set("Authorization", "Basic Y2xpZW50RGVtbzoxMjM0NTY=");
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    //body
    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
    map.add("code", code);
    map.add("grant_type", "authorization_code");
    map.add("redirect_uri", appProperties.getRedirectUri());
    map.add("scope", StringUtils.join(appProperties.getScopes()));
    return new HttpEntity(map, headers);
  }
}
