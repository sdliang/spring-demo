package com.demo.spring.boot.stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * Created by user-007 on 2017/11/27.
 */
@Resource
@RequestMapping("/stream")
public class StreamResource {

    private SendService sendService;

    @Autowired
    public void setSendService(SendService sendService) {
        this.sendService = sendService;
    }

    @RequestMapping("/send")
    public String send(String msg) {
        sendService.sendMsg(msg);
        return msg;
    }
}
