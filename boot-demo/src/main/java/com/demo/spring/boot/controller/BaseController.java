package com.demo.spring.boot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * Created by user-007 on 2017/7/13.
 */
public class BaseController {

    protected static final Logger LOGGER = LoggerFactory.getLogger(BaseController.class);

    protected RestTemplate restTemplate;

    @Autowired(required = false)
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
