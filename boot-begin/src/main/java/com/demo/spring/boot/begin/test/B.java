package com.demo.spring.boot.begin.test;

import lombok.Getter;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Getter
public class B {

  private A a;

  public B(@Lazy A a) {
    this.a = a;
  }

}
