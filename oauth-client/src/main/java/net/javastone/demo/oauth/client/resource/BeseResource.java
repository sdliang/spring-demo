package net.javastone.demo.oauth.client.resource;

import net.javastone.demo.oauth.client.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpSession;

public class BeseResource {

  public static final String USER_CONTEXT = "USER_CONTEXT";

  @Autowired
  private HttpSession session;

  protected UserContext getUserContext() {
    return (UserContext) session.getAttribute(USER_CONTEXT);
  }

  protected void setUserContext(UserContext userContext) {
    session.setAttribute(USER_CONTEXT, userContext);
  }
}
