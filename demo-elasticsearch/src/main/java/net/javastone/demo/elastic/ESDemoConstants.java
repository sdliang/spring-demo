package net.javastone.demo.elastic;

import lombok.experimental.UtilityClass;

/**
 * Created by sdliang on 2019/6/6.
 */
@UtilityClass
public class ESDemoConstants {

  public static final String INDEX_PRODUCTS = "products_v1";

  public static final String INDEX = "products";

  public static final String TYPE_DEFAULT = "_doc";
}
