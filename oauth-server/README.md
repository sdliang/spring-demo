授权码模式authorization_code：

    请求code
    curl -X GET http://localhost:8080/oauth/authorize?client_id=clientDemo&redirect_uri=http://localhost:8081/callback&response_type=code&scope=read_userinfo
    获取访问令牌
    curl -X POST http://localhost:8080/oauth/token -H "content-type: application/x-www-form-urlencoded" -d "code=8uYpdo&grant_type=authorization_code&redirect_uri=http%3A%2F%2Flocalhost%3A8081%2Fcallback&scope=read_userinfo"

用户密码模式password：

    curl -X POST --user clientDemo:123456 http://localhost:8080/oauth/token -H "accept: application/json" -H "content-type: application/x-www-form-urlencoded" -d "grant_type=password&username=sdliang&password=123456&scope=read_userinfo"

客户端模式client_credentials：

    curl -X POST --user clientDemo:123456 http://localhost:8080/oauth/token -H "accept: application/json" -H "content-type: application/x-www-form-urlencoded" -d "grant_type=client_credentials&scope=read_userinfo"

简化模式implicit：

    curl -X GET http://localhost:8080/oauth/authorize?client_id=clientDemo&redirect_uri=http://localhost:8081/callback&response_type=token&scope=read_userinfo
