package net.javastone.demo.oauth.client.service;

import net.javastone.demo.oauth.client.model.AccessToken;

public interface AccessTokenService {

  /**
   * 从oauth2服务器获取token
   *
   * @param code
   * @return
   */
  AccessToken getAccessToken(String code);
}
