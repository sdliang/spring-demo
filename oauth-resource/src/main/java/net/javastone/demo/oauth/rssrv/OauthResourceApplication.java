package net.javastone.demo.oauth.rssrv;

import net.javastone.demo.oauth.rssrv.config.AppProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

@SpringBootApplication
public class OauthResourceApplication {

  public static void main(String[] args) {
    SpringApplication.run(OauthResourceApplication.class, args);
  }

  /**
   * oauth2验证服务
   *
   * @return
   */
  @Bean
  public RemoteTokenServices remoteTokenServices(AppProperties properties) {
    RemoteTokenServices tokenServices = new RemoteTokenServices();
    tokenServices.setCheckTokenEndpointUrl(properties.getRemoteTokenEndpoint());
    tokenServices.setClientId(properties.getClientId());
    tokenServices.setClientSecret(properties.getClientSecret());
    return tokenServices;
  }
}
