package com.demo.spring.boot.transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.interceptor.DelegatingTransactionAttribute;
import org.springframework.transaction.interceptor.NoRollbackRuleAttribute;
import org.springframework.transaction.interceptor.RollbackRuleAttribute;
import org.springframework.transaction.interceptor.TransactionAttribute;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sdliang on 2018/3/27.
 */
public class SineduTransactionAttribute extends DelegatingTransactionAttribute {

  private static final Logger logger = LoggerFactory.getLogger(SineduTransactionAttribute.class);

  private List<RollbackRuleAttribute> rollbackRules;

  /**
   * Create a DelegatingTransactionAttribute for the given target attribute.
   *
   * @param targetAttribute the target TransactionAttribute to delegate to
   */
  public SineduTransactionAttribute(TransactionAttribute targetAttribute) {
    super(targetAttribute);
  }

  public List<RollbackRuleAttribute> getRollbackRules() {
    return rollbackRules;
  }

  public void setRollbackRules(List<RollbackRuleAttribute> rollbackRules) {
    this.rollbackRules = rollbackRules;
  }

  @Override
  public boolean rollbackOn(Throwable ex) {
    return super.rollbackOn(ex) || innerRollbackOn(ex);
  }

  public void addRollbackRule(RollbackRuleAttribute ruleAttribute) {
    if (getRollbackRules() == null) {
      setRollbackRules(new ArrayList<>());
    }
    getRollbackRules().add(ruleAttribute);
  }

  private boolean innerRollbackOn(Throwable ex) {
    if (logger.isTraceEnabled()) {
      logger.trace("Applying rules to determine whether transaction should rollback on " + ex);
    }

    RollbackRuleAttribute winner = null;
    int deepest = Integer.MAX_VALUE;

    if (this.rollbackRules != null) {
      for (RollbackRuleAttribute rule : this.rollbackRules) {
        int depth = rule.getDepth(ex);
        if (depth >= 0 && depth < deepest) {
          deepest = depth;
          winner = rule;
        }
      }
    }

    if (logger.isTraceEnabled()) {
      logger.trace("Winning rollback rule is: " + winner);
    }

    // User superclass behavior (rollback on unchecked) if no rule matches.
    if (winner == null) {
      logger.trace("No relevant rollback rule found: applying default rules");
      return super.rollbackOn(ex);
    }

    return !(winner instanceof NoRollbackRuleAttribute);
  }
}
