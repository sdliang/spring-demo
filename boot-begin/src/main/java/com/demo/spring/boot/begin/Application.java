package com.demo.spring.boot.begin;

import com.demo.spring.boot.begin.test.A;
import com.demo.spring.boot.begin.test.B;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

/**
 * Created by user-007 on 2017/11/14.
 */
@SpringBootApplication
//@ServletComponentScan
public class Application { // extends SpringBootServletInitializer

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Autowired
  private B b;

  @Autowired
  private A a;

  @PostConstruct
  public Object run() {
    System.out.println(b.getA());
    System.out.println(a);

    System.out.println(a.getB());
    System.out.println(b);


    return null;
  }
}
