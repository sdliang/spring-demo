package net.javastone.demo.eureka;

/**
 * Created by user-007 on 2017/11/8.
 */
public class Org {

    private String id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
