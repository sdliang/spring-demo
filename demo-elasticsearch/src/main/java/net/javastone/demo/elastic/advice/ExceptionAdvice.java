package net.javastone.demo.elastic.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Created by sdliang on 2019/5/31.
 */
@RestControllerAdvice
public class ExceptionAdvice {

  /**
   * 统一异常处理
   *
   * @param e
   * @return
   */
  @ExceptionHandler( {Exception.class})
  public ResponseEntity<String> exceptionMesage(Exception e) {
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
  }
}
