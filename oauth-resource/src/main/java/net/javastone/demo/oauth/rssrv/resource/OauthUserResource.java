package net.javastone.demo.oauth.rssrv.resource;

import net.javastone.demo.oauth.rssrv.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sdliang on 2018/10/31.
 */
@RestController
@RequestMapping("/user")
public class OauthUserResource {


  @GetMapping("/{id}")
  public ResponseEntity<User> userInfo(@PathVariable("id") String id) {
    String username = (String) SecurityContextHolder.getContext().getAuthentication()
                                                    .getPrincipal();
    return ResponseEntity.ok(new User(id, username, 20));

  }

  @GetMapping("/current")
  public ResponseEntity<String> userInfo() {
    String username = (String) SecurityContextHolder.getContext().getAuthentication()
                                                    .getPrincipal();
    return ResponseEntity.ok(username);

  }
}
