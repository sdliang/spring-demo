package net.javastone.demo.elastic.resource.rs;

import net.javastone.demo.elastic.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sdliang on 2019/5/31.
 */
@RestController
public class ProductResource {

  @Autowired
  private ProductService productService;


  public static void main(String[] args) {
    String str = toBinary(51);
    System.out.println(str);
    str = toString(51, 2);
    System.out.println(str);
  }

  /**
   * 转成二进制
   *
   * @param num 数值
   * @return
   */
  private static String toBinary(int num) {
    StringBuilder sb = new StringBuilder();

    while (num > 0) {
      sb.append(num & 1);
      num >>>= 1;
    }

    return sb.reverse().toString();
  }

  /**
   * 数字转成字符串
   *
   * @param num   数字
   * @param scale 进制
   * @return
   */
  private static String toString(int num, int scale) {
    StringBuilder sb = new StringBuilder();

    while (num > 0) {
      sb.append(num % scale);
      num /= scale;
    }

    return sb.reverse().toString();
  }


}
