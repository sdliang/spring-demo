package net.javastone.demo.oauth.client.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "app")
public class AppProperties {
  /**
   * token获取地址
   */
  private String   accessTokenUri;
  /**
   * 资源服务器地址
   */
  private String   resourceUri;
  /**
   * 授权回调地址
   */
  private String   redirectUri;
  /**
   * 应用ID
   */
  private String   appId;
  /**
   * 应用密钥
   */
  private String   appSecret;
  /**
   * 授权范围
   */
  private String[] scopes;
}
