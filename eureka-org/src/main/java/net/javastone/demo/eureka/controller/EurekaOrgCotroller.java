package net.javastone.demo.eureka.controller;

import net.javastone.demo.eureka.Org;
import net.javastone.demo.eureka.properties.RemoteProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

/**
 * Created by user-007 on 2017/11/8.
 */
@RestController
@RequestMapping("/eureka/org")
public class EurekaOrgCotroller {

    private Environment environment;

    private RestTemplate restTemplate;

    private RemoteProperties remoteProperties;

    @Value("${version}")
    private String version;

    @Autowired(required = false)
    public void setRemoteProperties(RemoteProperties remoteProperties) {
        this.remoteProperties = remoteProperties;
    }

    @Autowired(required = false)
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Autowired(required = false)
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @RequestMapping("/{name}")
    public Org home(@PathVariable("name") String name) {
        Org org = new Org();
        org.setId(UUID.randomUUID().toString());
        org.setName(name);
        return org;
    }

    @RequestMapping("/user/{name}")
    public String hiUser(@PathVariable("name") String name) {
        return restTemplate.getForObject("http://srv-user/eureka/user/" + name, String.class);
    }

    @RequestMapping("/config/remote")
    public String configRemote() {
        return "class:" + remoteProperties.getVersion() + "; controller:" + version;
    }
}
