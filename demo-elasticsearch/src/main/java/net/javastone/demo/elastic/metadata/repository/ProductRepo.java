package net.javastone.demo.elastic.metadata.repository;

import net.javastone.demo.elastic.metadata.model.Product;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by sdliang on 2019/5/31.
 */
@Repository
public interface ProductRepo extends ElasticsearchRepository<Product, String> {

}
