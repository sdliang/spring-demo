package com.demo.spring.boot.begin.test;

import lombok.Getter;
import org.springframework.stereotype.Component;

@Component
@Getter
public class A {

  private B b;

  public A(B b) {
    this.b = b;
  }
}
