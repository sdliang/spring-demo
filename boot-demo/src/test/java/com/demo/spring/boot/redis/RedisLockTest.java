package com.demo.spring.boot.redis;

import com.demo.spring.boot.TestBase;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by user-007 on 2017/11/24.
 */
public class RedisLockTest extends TestBase {

    private StringRedisTemplate redisTemplate;

    @Autowired
    public void setRedisTemplate(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testLock() throws Exception {
        final String lockKey = UUID.randomUUID().toString();
        final long[] contAry = {0};
        int threadSize = 10;

        ExecutorService executorService = Executors.newFixedThreadPool(threadSize);
        List<Future> futureList = new ArrayList<>(threadSize);

        for (int i = 0; i < threadSize; i++) {
            futureList.add(executorService.submit(new CallableTest(i) {
                @Override
                public Long call() throws Exception {
                    System.out.println("execute thread:" + getId());
                    RedisLock lock = new RedisLock(redisTemplate, lockKey);
                    try {
                        lock.lock();
                        contAry[0]++;
                    } finally {
                        lock.unlock();
                    }
                    System.out.println("thread:" + getId() + "result:" + contAry[0]);
                    return contAry[0];
                }
            }));
        }
//        for (Future future : futureList) {
//            System.out.println("final result:----------" + future.get());
//        }
        executorService.shutdown();
    }

    abstract class CallableTest implements Callable<Long> {
        private int id;

        public int getId() {
            return id;
        }

        public CallableTest(int id) {
            this.id = id;
        }
    }
}
