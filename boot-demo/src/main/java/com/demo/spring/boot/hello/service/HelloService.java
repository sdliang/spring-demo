package com.demo.spring.boot.hello.service;

import com.demo.spring.boot.hello.pojo.Hello;

import java.util.List;

/**
 * Created by BlueDream on 2017/11/6.
 */
public interface HelloService {

    String save(Hello hello);

    void update(Hello hello);

    Hello delete(String id);

    Hello get(String id);

    List<Hello> queryByName(String name);
}
