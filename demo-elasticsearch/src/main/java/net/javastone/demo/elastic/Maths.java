package net.javastone.demo.elastic;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Maths {

  public static void main(String[] args) {
    double sequ = sequare(25, 0.001);
    System.out.println(sequ);

    int[] ary = new int[] {6, 3, 1, 6, 1, 1, 9, 3, 10, 5, 2};
    quickSort(ary, 0, ary.length - 1);
    for (int i : ary) {
      System.out.println(i);
    }
    int[] seed = new int[] {1, 2, 3, 5, 7, 9};
    List<Resolve> list = arrange(seed, 21);
    list.forEach(Resolve::print);
  }

  /**
   * 计算近似平方根(二分法)
   *
   * @param num        数值
   * @param deltaRound 误差 范围
   * @return
   */
  public static double sequare(double num, double deltaRound) {

    double min = 0;
    double max = num;

    if (num <= 0) {
      return -1;
    }

    double middle = 0;
    double sequ;

    for (int i = 0; i < 10000; i++) {

      middle = (min + max) / 2;
      sequ = middle * middle;

      if (deltaRound >= Math.abs((sequ / num) - 1)) {
        return middle;
      } else {
        if (sequ > num) {
          max = middle;
        } else {
          min = middle;
        }
      }
    }

    return middle;
  }

  /**
   * 快速排序法
   *
   * @param ary
   * @param begin
   * @param end
   */
  public static void quickSort(int[] ary, int begin, int end) {
    //参数判断
    int len = end - begin;
    if (len <= 0) {
      return;
    }
    //默认参数
    if (end == 0) {
      end = ary.length - 1;
    }

    int seedIdx = getSeedIndex(ary, begin, end);
    int leftIdx = begin - 1;
    int rightIdx = end;
    int seed = ary[seedIdx];
    //交换seed和end
    //    changePosition(ary, seedIdx, end);

    for (int i = begin; i < rightIdx; i++) {
      //如果当前等于seed, 不动
      if (ary[i] == seed) {
        continue;
      }
      if (ary[i] < seed) {
        //如果当前小于seed, 左移
        leftIdx++;
        if (i > leftIdx) {
          changePosition(ary, leftIdx, i);
        }
      } else {
        //如果当前大于seed, 右移
        rightIdx--;
        if (i < rightIdx) {
          changePosition(ary, i, rightIdx);
          i--;
        }
      }
    }
    //最后一个判断
    //    if (rightIdx > 0 && ary[rightIdx - 1] > seed) {
    //      changePosition(ary, rightIdx - 1, end);
    //    }

    //    if (len <= 1) {
    //      return;
    //    }
    for (int i : ary) {
      System.out.println(i);
    }
    System.out.println("---------------");
    if (leftIdx > begin) {
      quickSort(ary, begin, leftIdx);
    }
    if (end > rightIdx) {
      quickSort(ary, rightIdx, end);
    }

  }

  /**
   * 对调位置
   *
   * @param ary  数组
   * @param from 从
   * @param to   到
   */
  private static void changePosition(int[] ary, int from, int to) {
    if (from == to) {
      return;
    }
    int tmp = ary[from];
    ary[from] = ary[to];
    ary[to] = tmp;
  }

  /**
   * 获取种子数量
   *
   * @param ary
   * @param begin
   * @param end
   * @return
   */
  private static int getSeedIndex(int[] ary, int begin, int end) {
    return (begin + end) / 2;
  }

  /**
   * 分配
   *
   * @param seed 限制组
   * @param sum  大小
   * @return
   */
  public static List<Resolve> arrange(int[] seed, int sum) {
    List<Resolve> list = new ArrayList<>();

    nextResolve(list, new Resolve(seed), sum, 0);

    return list;
  }

  private static void nextResolve(List<Resolve> list, Resolve last, int sum, int startIdx) {

    Resolve next;
    int tmp;
    int tmpSum = last.sum();
    int seedLen = last.seed.length;
    for (int i = startIdx; i < seedLen; i++) {
      tmp = tmpSum + last.seed[i];
      if (tmp < sum) {
        next = last.copyAndIncrease(i);
        nextResolve(list, next, sum, i);
      } else if (tmp == sum) {
        list.add(last.copyAndIncrease(i));
      } else {
        break;
      }
    }
  }

  @Getter
  @Setter
  private static class Resolve {

    private int[] seed;
    private int[] num;

    public Resolve(int[] seed) {
      this.seed = seed;
      this.num = new int[seed.length];
      for (int i = 0; i < this.num.length; i++) {
        this.num[i] = 0;
      }
    }

    public int sum() {
      int sum = 0;
      for (int i = 0; i < seed.length; i++) {
        sum += seed[i] * num[i];
      }
      return sum;
    }

    public Resolve copy() {
      Resolve inst = new Resolve(seed);

      inst.setNum(Arrays.copyOf(num, num.length));

      return inst;
    }

    public Resolve copyAndIncrease(int idx) {
      Resolve inst = copy();

      inst.num[idx]++;

      return inst;
    }

    public void print() {
      StringBuffer sb = new StringBuffer("[");
      for (int i = 0; i < seed.length; i++) {
        for (int k = 0; k < num[i]; k++) {
          sb.append(seed[i]).append(" ");
        }
      }
      sb.append("]");
      System.out.println(sb.toString());
    }
  }
}
