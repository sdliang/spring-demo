package com.demo.spring.boot.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by user-007 on 2017/11/7.
 */
@Component
@PropertySource("classpath:properties/transaction.properties")
@ConfigurationProperties
public class TransactionProperties {

    private String pointcut;
    private String rollbackFor;
    private int timeout;
    private String[] required;
    private String[] support;
    private String[] requiresNew;

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getPointcut() {
        return pointcut;
    }

    public void setPointcut(String pointcut) {
        this.pointcut = pointcut;
    }

    public String getRollbackFor() {
        return rollbackFor;
    }

    public void setRollbackFor(String rollbackFor) {
        this.rollbackFor = rollbackFor;
    }

    public String[] getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required.split(",");
    }

    public String[] getSupport() {
        return support;
    }

    public void setSupport(String support) {
        this.support = support.split(",");
    }

    public String[] getRequiresNew() {
        return requiresNew;
    }

    public void setRequiresNew(String requiresNew) {
        this.requiresNew = requiresNew.split(",");
    }
}
