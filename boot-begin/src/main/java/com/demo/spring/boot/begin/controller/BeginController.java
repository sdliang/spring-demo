package com.demo.spring.boot.begin.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by user-007 on 2017/11/14.
 */
@RestController
@RequestMapping("/begin")
public class BeginController {

    private static final String STR = "hello %s";

    @RequestMapping("/hello/{name}")
    public String hello(@PathVariable("name") String name) {
        return String.format(STR, name);
    }
}
