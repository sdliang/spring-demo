package com.demo.spring.boot.hello.controller;

import com.demo.spring.boot.ControllerTestBase;
import com.demo.spring.boot.hello.pojo.Hello;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

/**
 * Created by BlueDream on 2017/11/6.
 */
public class HelloControllerTest extends ControllerTestBase {

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void testHelloName() throws Exception {
        String name = "sdliang";
        ResponseEntity<String> responseEntity =
                template.getForEntity(base.toString() + "hello/name/" + name, String.class);

        Assert.assertEquals(String.format(HelloBootController.TEMPLATE, name),
                responseEntity.getBody());
    }

    @Test
    public void testHelloBody() throws Exception {
        Hello hello = new Hello();
        hello.setId(UUID.randomUUID().toString());
        hello.setName("sdliang");
        hello.setAge(18);
        hello.setMale(true);
        ResponseEntity<String> responseEntity =
                template.postForEntity(base.toString() + "hello/body", hello, String.class);

        Assert.assertEquals(String.format(HelloBootController.TEMPLATE_BODY,
                        hello.getName(), hello.getMale() ? "男" : "女", hello.getAge()),
                responseEntity.getBody());
    }

    @Test
    public void testAppConfig() throws Exception {
        String key = "domain";
        ResponseEntity<String> responseEntity =
                template.getForEntity(base.toString() + "hello/config/" + key, String.class);

        System.out.println(responseEntity.getBody());
        Assert.assertEquals("http://localhost:9090/springboot/",
                responseEntity.getBody());
    }

}
