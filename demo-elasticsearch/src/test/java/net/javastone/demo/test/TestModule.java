package net.javastone.demo.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by sdliang on 2019/5/31.
 */
@Configuration
@ComponentScan(basePackages = "net.javastone.demo")
public class TestModule {

}
