package net.javastone.demo.elastic.metadata.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by sdliang on 2019/6/6.
 */

@Getter
@Setter
public class Category {

  private String id;

  private String name;
}
