package com.demo.spring.boot.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

/**
 * Created by user-007 on 2017/11/8.
 */
@Repository
public class RedisService {

    private StringRedisTemplate redisTemplate;

    @Autowired(required = false)
    public void setRedisTemplate(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    public void save(String key, String value) {
        ValueOperations<String, String> valOpts = redisTemplate.opsForValue();
        valOpts.set(key, value);
    }

    public String get(String key) {
        ValueOperations<String, String> valOpts = redisTemplate.opsForValue();
        return valOpts.get(key);
    }
}
