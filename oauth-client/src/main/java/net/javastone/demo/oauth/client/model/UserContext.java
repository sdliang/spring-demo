package net.javastone.demo.oauth.client.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserContext {

  private String username;
}
