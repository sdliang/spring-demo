package net.javastone.demo.elastic.metadata.repository;

import org.elasticsearch.cluster.metadata.AliasMetaData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.AliasBuilder;
import org.springframework.data.elasticsearch.core.query.AliasQuery;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by sdliang on 2019/5/31.
 */
@Repository
public class IndexRepo {

  @Autowired
  private ElasticsearchTemplate template;

  /**
   * 创建索引
   *
   * @param indexName
   * @return
   */
  public boolean createIndex(String indexName) {
    return template.createIndex(indexName);
  }

  /**
   * 创建索引
   *
   * @param claz
   * @return
   */
  public boolean createIndex(Class claz) {
    return template.createIndex(claz);
  }

  /**
   * 创建索引
   *
   * @param indexName
   * @param aliasName 别名
   * @return
   */
  public boolean createIndex(String indexName, String aliasName) {
    boolean flag = template.createIndex(indexName);
    if (flag) {
      flag = addAlias(indexName, aliasName);
    }
    return flag;
  }

  /**
   * 创建索引
   *
   * @param claz
   * @param aliasName 别名
   * @return
   */
  public boolean createIndex(Class claz, String aliasName) {
    boolean flag = template.createIndex(claz);
    if (flag) {
      flag = addAlias(getIndexName(claz), aliasName);
    }
    return flag;
  }

  /**
   * 删除索引
   *
   * @param indexName
   * @return
   */
  public boolean deleteIndex(String indexName) {
    return template.deleteIndex(indexName);
  }

  /**
   * 删除索引
   *
   * @param claz
   * @return
   */
  public boolean deleteIndex(Class claz) {
    return template.deleteIndex(claz);
  }

  /**
   * 索引是否存在
   *
   * @param indexName
   * @return
   */
  public boolean indexExists(String indexName) {
    return template.indexExists(indexName);
  }

  /**
   * 索引是否存在
   *
   * @param claz
   * @return
   */
  public boolean indexExists(Class claz) {
    return template.indexExists(claz);
  }

  /**
   * 添加映射
   *
   * @param indexName
   * @param type
   * @param mapping
   * @return
   */
  public boolean putMapping(String indexName, String type, Object mapping) {
    return template.putMapping(indexName, type, mapping);
  }

  /**
   * 添加映射
   *
   * @param mappingClaz
   * @return
   */
  public boolean putMapping(Class mappingClaz) {
    return template.putMapping(mappingClaz);
  }

  /**
   * 获取映射
   *
   * @param mappingClaz
   * @return
   */
  public Map<String, Object> getMapping(Class mappingClaz) {
    return template.getMapping(mappingClaz);
  }

  /**
   * 获取映射
   *
   * @param indexName
   * @param type
   * @return
   */
  public Map<String, Object> getMapping(String indexName, String type) {
    return template.getMapping(indexName, type);
  }

  /**
   * 增加别名映射
   *
   * @param aliasQuery
   * @return
   */
  public Boolean addAlias(AliasQuery aliasQuery) {
    return template.addAlias(aliasQuery);
  }

  /**
   * 删除别名映射
   *
   * @param aliasQuery
   * @return
   */
  public boolean removeAlias(AliasQuery aliasQuery) {
    return template.removeAlias(aliasQuery);
  }

  /**
   * 增加别名映射
   *
   * @param indexName
   * @param aliasName
   * @return
   */
  public Boolean addAlias(String indexName, String aliasName) {
    AliasBuilder builder = new AliasBuilder();
    builder.withIndexName(indexName).withAliasName(aliasName);
    return template.addAlias(builder.build());
  }

  /**
   * 删除别名映射
   *
   * @param indexName
   * @param aliasName
   * @return
   */
  public boolean removeAlias(String indexName, String aliasName) {
    AliasBuilder builder = new AliasBuilder();
    builder.withIndexName(indexName).withAliasName(aliasName);
    return template.removeAlias(builder.build());
  }

  /**
   * 查询别名映射
   *
   * @param indexName
   * @return
   */
  public List<AliasMetaData> queryForAlias(String indexName) {
    return template.queryForAlias(indexName);
  }

  /**
   * 获取索引名称
   *
   * @param claz
   * @return
   */
  private String getIndexName(Class claz) {
    if (!claz.isAnnotationPresent(Document.class)) {
      throw new RuntimeException(String.format("找不到@Document注解: %s", claz.getName()));
    }
    Document document = (Document) claz.getAnnotation(Document.class);
    return document.indexName();
  }

}
