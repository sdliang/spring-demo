package net.javastone.demo.oauth.rssrv.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by sdliang on 2018/10/31.
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "app")
public class AppProperties {

  /**
   * 远程验证地址
   */
  private String   remoteTokenEndpoint;
  /**
   * APPID
   */
  private String   clientId;
  /**
   * APP密钥
   */
  private String   clientSecret;
  /**
   * 授权过滤的地址
   */
  private String[] oauthUris;

}
