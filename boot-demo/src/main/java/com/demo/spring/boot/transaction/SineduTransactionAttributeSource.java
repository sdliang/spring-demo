package com.demo.spring.boot.transaction;

import org.springframework.core.MethodClassKey;
import org.springframework.lang.Nullable;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.interceptor.NoRollbackRuleAttribute;
import org.springframework.transaction.interceptor.RollbackRuleAttribute;
import org.springframework.transaction.interceptor.TransactionAttribute;
import org.springframework.transaction.interceptor.TransactionAttributeSource;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by sdliang on 2018/3/27.
 */
public class SineduTransactionAttributeSource implements TransactionAttributeSource {

  private TransactionAttributeSource attributeSource;

  private final static TransactionAttribute NULL_TRANSACTION_ATTRIBUTE = new DefaultTransactionAttribute();

  private final Map<Object, TransactionAttribute> attributeCache = new ConcurrentHashMap<>(1024);

  private SineduTransactionProps sineduTransactionProps;

  public SineduTransactionAttributeSource(TransactionAttributeSource attributeSource,
                                          SineduTransactionProps sineduTransactionProps) {
    this.attributeSource = attributeSource;
    this.sineduTransactionProps = sineduTransactionProps;
  }

  @Override
  public TransactionAttribute getTransactionAttribute(Method method,
                                                      @Nullable Class<?> targetClass) {
    if (method.getDeclaringClass() == Object.class) {
      return null;
    }

    // First, see if we have a cached value.
    Object cacheKey = getCacheKey(method, targetClass);
    Object cached = this.attributeCache.get(cacheKey);
    if (cached != null) {
      // Value will either be canonical value indicating there is no transaction attribute,
      // or an actual transaction attribute.
      if (cached == NULL_TRANSACTION_ATTRIBUTE) {
        return null;
      } else {
        return (TransactionAttribute) cached;
      }
    } else {
      TransactionAttribute attribute = getDelegateTransactionAttribute(method, targetClass);
      if (attribute == null) {
        this.attributeCache.put(cacheKey, NULL_TRANSACTION_ATTRIBUTE);
      } else {
        this.attributeCache.put(cacheKey, attribute);
      }
      return attribute;
    }
  }

  /**
   * Determine a cache key for the given method and target class.
   * <p>Must not produce same key for overloaded methods.
   * Must produce same key for different instances of the same method.
   *
   * @param method      the method (never {@code null})
   * @param targetClass the target class (may be {@code null})
   * @return the cache key (never {@code null})
   */
  protected Object getCacheKey(Method method, @Nullable Class<?> targetClass) {
    return new MethodClassKey(method, targetClass);
  }

  private TransactionAttribute getDelegateTransactionAttribute(Method method,
                                                               @Nullable Class<?> targetClass) {
    TransactionAttribute originAttr = attributeSource.getTransactionAttribute(method, targetClass);

    if (originAttr != null && sineduTransactionProps != null) {
      SineduTransactionAttribute attribute = new SineduTransactionAttribute(originAttr);
      //add rollback Exception
      if (sineduTransactionProps.getRollbackFors() != null) {
        sineduTransactionProps.getRollbackFors()
            .forEach(aClass -> attribute.addRollbackRule(new RollbackRuleAttribute(aClass)));
      }
      //add no rollback Exception
      if (sineduTransactionProps.getNoRollbacks() != null) {
        sineduTransactionProps.getNoRollbacks()
            .forEach(aClass -> attribute.addRollbackRule(new NoRollbackRuleAttribute(aClass)));
      }
      originAttr = attribute;
    }
    return originAttr;
  }

}
