package com.demo.spring.boot.hello.controller;

import com.demo.spring.boot.controller.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user-007 on 2017/11/8.
 */
@RestController
@RequestMapping("/file")
public class FileUploadController extends BaseController {

    private List<String> files = new ArrayList<>();

    @RequestMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile file) {
        String fileName = file.getOriginalFilename();
        if (files.contains(fileName)) {
            throw new RuntimeException("文件已存在:" + fileName);
        }
        files.add(fileName);
        return "上传成功:" + fileName;
    }

    @RequestMapping("/list")
    public List<String> list() {
        return files;
    }
}
