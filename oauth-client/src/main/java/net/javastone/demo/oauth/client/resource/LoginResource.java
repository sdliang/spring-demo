package net.javastone.demo.oauth.client.resource;

import net.javastone.demo.oauth.client.model.User;
import net.javastone.demo.oauth.client.model.UserContext;
import net.javastone.demo.oauth.client.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@RequestMapping("/login")
public class LoginResource extends BeseResource {

  @Autowired
  private UserService userService;


  /**
   * 登陆接口
   *
   * @param code 授权码
   * @return
   */
  @GetMapping("/login")
  public ResponseEntity<User> login(String code) {
    User user = userService.remoteGet(code);
    if (user == null) {
      throw new RuntimeException("找不到用户信息");
    }
    //设置上下文
    if (getUserContext() == null || !getUserContext().getUsername().equals(user.getUsername())) {
      UserContext userContext = new UserContext();
      userContext.setUsername(user.getUsername());
      setUserContext(userContext);
    }

    return ResponseEntity.ok(user);
  }

  /**
   * 当前用户信息
   *
   * @return
   */
  @GetMapping("/current")
  public ResponseEntity<User> getUser() {
    if (getUserContext() == null) {
      throw new RuntimeException("用户未登陆");
    }
    User user = userService.get(getUserContext().getUsername());
    if (user == null) {
      throw new RuntimeException("找不到用户信息");
    }
    return ResponseEntity.ok(user);
  }
}
