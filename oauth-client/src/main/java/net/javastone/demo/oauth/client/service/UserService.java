package net.javastone.demo.oauth.client.service;

import net.javastone.demo.oauth.client.model.User;

public interface UserService {
  /**
   * 根据username获取用户信息
   *
   * @param username 用户名
   * @return
   */
  User get(String username);

  /**
   * 从资源服务器获取用户信息
   *
   * @param code 授权码
   * @return
   */
  User remoteGet(String code);

}
