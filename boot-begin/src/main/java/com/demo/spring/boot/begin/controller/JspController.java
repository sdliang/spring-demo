package com.demo.spring.boot.begin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by user-007 on 2017/11/14.
 */
@Controller
@RequestMapping("/jsp")
public class JspController {

    @RequestMapping("/hello")
    public ModelAndView hello() {
        return new ModelAndView("hello");
    }
}
