package com.demo.spring.boot.config;

import com.demo.spring.boot.properties.TransactionProperties;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.interceptor.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sdliang on 2017/8/28.
 */
@Aspect
@Configuration
public class TxAdviceConfig {

    /**
     * AOP事務
     *
     * @param transactionManager
     * @param transactionProperties
     * @return
     * @throws ClassNotFoundException
     */
    @Bean
    public Advisor advisor(PlatformTransactionManager transactionManager,
                           TransactionProperties transactionProperties) throws ClassNotFoundException {
        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
        pointcut.setExpression(transactionProperties.getPointcut());
        return new DefaultPointcutAdvisor(pointcut,
                transactionInterceptor(transactionManager, transactionProperties));
    }

    /**
     * 事务拦截
     *
     * @param transactionManager
     * @param transactionProperties
     * @return
     * @throws ClassNotFoundException
     */
    private TransactionInterceptor transactionInterceptor(PlatformTransactionManager transactionManager,
                                                          TransactionProperties transactionProperties) throws ClassNotFoundException {
        Map<String, TransactionAttribute> txMap = new HashMap<>();

        /*当前存在事务就使用当前事务，当前不存在事务就创建一个新的事务*/
        requiredAttribute(txMap, transactionProperties);

         /*只读事务，不做更新操作*/
        supportedAttribute(txMap, transactionProperties);

         /*独立事务*/
        requireNewAttribute(txMap, transactionProperties);

        NameMatchTransactionAttributeSource source = new NameMatchTransactionAttributeSource();
        source.setNameMap(txMap);
        TransactionInterceptor txAdvice = new TransactionInterceptor(transactionManager, source);
        return txAdvice;
    }

    /**
     * 只读事务
     *
     * @return
     */
    private void supportedAttribute(Map<String, TransactionAttribute> txMap,
                                    TransactionProperties transactionProperties) {
        if (transactionProperties.getSupport() == null || transactionProperties.getSupport().length <= 0) {
            return;
        }
        RuleBasedTransactionAttribute readOnlyTx = new RuleBasedTransactionAttribute();
        readOnlyTx.setPropagationBehavior(TransactionDefinition.PROPAGATION_NOT_SUPPORTED);
        readOnlyTx.setReadOnly(true);
        for (String support : transactionProperties.getSupport()) {
            txMap.put(support, readOnlyTx);
        }
    }

    /**
     * 默认事务(REQUIRED)
     *
     * @return
     */
    private void requiredAttribute(Map<String, TransactionAttribute> txMap,
                                   TransactionProperties transactionProperties) throws ClassNotFoundException {
        if (transactionProperties.getRequired() == null || transactionProperties.getRequired().length <= 0) {
            return;
        }
        RuleBasedTransactionAttribute requiredTx = new RuleBasedTransactionAttribute();
        //回滚设置
        requiredTx.setRollbackRules(Collections.singletonList(
                new RollbackRuleAttribute(Class.forName(transactionProperties.getRollbackFor()))));
        //默认事务行为
        requiredTx.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
        requiredTx.setTimeout(transactionProperties.getTimeout());
        //设置配置
        for (String required : transactionProperties.getRequired()) {
            txMap.put(required, requiredTx);
        }
    }

    /**
     * 独立事务
     *
     * @return
     */
    private void requireNewAttribute(Map<String, TransactionAttribute> txMap,
                                     TransactionProperties transactionProperties) throws ClassNotFoundException {
        if (transactionProperties.getRequiresNew() == null || transactionProperties.getRequiresNew().length <= 0) {
            return;
        }
        RuleBasedTransactionAttribute requiredNewTx = new RuleBasedTransactionAttribute();
        //回滚设置
        requiredNewTx.setRollbackRules(Collections.singletonList(
                new RollbackRuleAttribute(Class.forName(transactionProperties.getRollbackFor()))));
        //默认事务行为
        requiredNewTx.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        requiredNewTx.setTimeout(transactionProperties.getTimeout());
        //设置
        for (String requireNew : transactionProperties.getRequiresNew()) {
            txMap.put(requireNew, requiredNewTx);
        }
    }
}