package net.javastone.demo.elastic.service;

import net.javastone.demo.elastic.metadata.repository.IndexRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by sdliang on 2019/5/31.
 */
@Service
public class IndexServiceImpl implements IndexService {

  @Autowired
  private IndexRepo indexRepo;

  @Override
  public boolean create(String name) {
    return indexRepo.createIndex(name);
  }

  @Override
  public boolean create(String indexName, String aliasName) {
    return indexRepo.createIndex(indexName, aliasName);
  }

  @Override
  public boolean indexExists(String name) {
    return indexRepo.indexExists(name);
  }

  @Override
  public boolean delete(String name) {
    return indexRepo.deleteIndex(name);
  }

  @Override
  public boolean addAlias(String indexName, String aliasName) {
    return indexRepo.addAlias(indexName, aliasName);
  }

  @Override
  public boolean deleteAlias(String indexName, String aliasName) {
    return indexRepo.removeAlias(indexName, aliasName);
  }
}
