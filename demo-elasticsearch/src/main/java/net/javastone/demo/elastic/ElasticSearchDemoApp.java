package net.javastone.demo.elastic;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

/**
 * Created by sdliang on 2019/5/31.
 */
@SpringBootApplication
@EnableElasticsearchRepositories
public class ElasticSearchDemoApp {

  public static void main(String[] args) {
    new SpringApplicationBuilder(ElasticSearchDemoApp.class).web(WebApplicationType.SERVLET).build()
        .run(args);
  }
}
