package net.javastone.demo.eureka.controller;

import net.javastone.demo.eureka.Org;
import net.javastone.demo.eureka.feign.OrgApi;
import net.javastone.demo.eureka.properties.RemoteProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by user-007 on 2017/11/8.
 */
@RestController
@RequestMapping("/eureka/user")
public class EurekaUserCotroller {

    private Environment environment;

    private OrgApi orgApi;

    private RemoteProperties remoteProperties;

    @Autowired(required = false)
    public void setRemoteProperties(RemoteProperties remoteProperties) {
        this.remoteProperties = remoteProperties;
    }

    @Autowired(required = false)
    public void setOrgApi(OrgApi orgApi) {
        this.orgApi = orgApi;
    }

    @Autowired(required = false)
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @RequestMapping("/{name}")
    public String home(@PathVariable("name") String name) {
        return "hi " + name + ",i am from "
                + environment.getProperty("spring.application.name")
                + ":" + environment.getProperty("server.port");
    }

    @RequestMapping("/org/{name}")
    public Org hiOrg(@PathVariable("name") String name) {
        return orgApi.hiOrg(name);
    }

    @RequestMapping("/config/remote")
    public String configRemote() {
        return remoteProperties.getVersion();
    }
}
