package net.javastone.demo.oauth.rssrv.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * Created by sdliang on 2018/10/31.
 */
@Configuration
@EnableResourceServer
public class OAuth2ResourceServer extends ResourceServerConfigurerAdapter {

  @Autowired
  private AppProperties appProperties;

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests().anyRequest().authenticated().and().requestMatchers()
        .antMatchers(appProperties.getOauthUris());
//    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
//        .and().requestMatchers().anyRequest()
//        .and().anonymous()
//        .and().authorizeRequests().antMatchers(appProperties.getOauthUris());
  }
}
