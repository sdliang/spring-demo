package com.demo.spring.boot.transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sdliang on 2018/1/19.
 */
public class SineduTransactionProps {

  private List<Class<?>> noRollbacks;

  private List<Class<?>> rollbackFors;

  public List<Class<?>> getNoRollbacks() {
    return noRollbacks;
  }

  public List<Class<?>> getRollbackFors() {
    return rollbackFors;
  }

  public void setNoRollbacks(List<Class<?>> noRollbacks) {
    this.noRollbacks = noRollbacks;
  }

  public void setRollbackFors(List<Class<?>> rollbackFors) {
    this.rollbackFors = rollbackFors;
  }

  public void addRollbackFor(Class<? extends Exception> exceptionClass) {
    if (getRollbackFors() == null) {
      setRollbackFors(new ArrayList<>());
    }
    if (!getRollbackFors().contains(exceptionClass)) {
      getRollbackFors().add(exceptionClass);
    }
  }

  public void addNoRollback(Class<? extends Exception> exceptionClass) {
    if (getNoRollbacks() == null) {
      setNoRollbacks(new ArrayList<>());
    }
    if (!getNoRollbacks().contains(exceptionClass)) {
      getNoRollbacks().add(exceptionClass);
    }
  }
}
