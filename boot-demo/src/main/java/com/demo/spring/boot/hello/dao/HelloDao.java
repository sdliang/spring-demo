package com.demo.spring.boot.hello.dao;

import com.demo.spring.boot.hello.pojo.Hello;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by BlueDream on 2017/11/6.
 */
public interface HelloDao extends JpaRepository<Hello, String>{

    List<Hello> findByName(String name);

}
