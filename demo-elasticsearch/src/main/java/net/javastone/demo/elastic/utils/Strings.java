package net.javastone.demo.elastic.utils;

import lombok.experimental.UtilityClass;

import java.util.UUID;

/**
 * Created by sdliang on 2019/6/6.
 */
@UtilityClass
public class Strings {

  /**
   * 唯一标志
   *
   * @return
   */
  public static String uuid() {
    return UUID.randomUUID().toString().replaceAll("-", "");
  }
}
