package net.javastone.demo.oauthserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;

/**
 * Created by sdliang on 2018/10/31.
 */
@Configuration
@EnableAuthorizationServer
public class OAuth2AuthorizationServer extends AuthorizationServerConfigurerAdapter {

  @Autowired
  private AppProperties appProperties;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private RedisConnectionFactory connectionFactory;

  @Override
  public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
    endpoints.authenticationManager(authenticationManager)//用户名密码模式
             .tokenStore(new RedisTokenStore(connectionFactory));
  }

  @Override
  public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    //TODO: 改从数据库获取
    clients.inMemory().withClient(appProperties.getClientId())
           .secret(appProperties.getClientSecret()).redirectUris(appProperties.getClientCallback())
           .authorizedGrantTypes(appProperties.getGrantTypes())
           .accessTokenValiditySeconds(appProperties.getTokenValidSeconds())
           .scopes(appProperties.getScopes());
  }

  @Override
  public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
    //允许表单认证
    security.allowFormAuthenticationForClients();
    //远程验证
    security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
  }

  public static void main(String[] args) {
    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    System.out.println(encoder
                           .matches("123456", "$2a$10$CyAbhuZEfGr1Rn5OsaK/U.jitPYKdylo5AyW2RmVWOdAVIAsLblxa"));
  }
}
