package com.demo.spring.boot.redis;

import com.demo.spring.boot.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by user-007 on 2017/7/13.
 */
@RestController
public class StringRedisController extends BaseController {

    private RedisService redisService;

    @Autowired(required = false)
    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }

    @RequestMapping("/redis/set/{key}/{value}")
    public String setKeyValue(@PathVariable String key,
                              @PathVariable String value) {
        LOGGER.info("set redis:key={},value={}", key, value);
        redisService.save(key, value);
        return "ok";
    }

    @RequestMapping("/redis/get/{key}")
    public String getValue(@PathVariable String key) {
        LOGGER.info("get redis:key={}", key);
        return redisService.get(key);
    }

}
