package net.javastone.demo.oauth.client.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessToken {
  /**
   * token
   */
  private String access_token;
  /**
   * 刷新token
   */
  private String refresh_token;
  /**
   * token类型
   */
  private String token_type;
  /**
   * 有效时长(秒)
   */
  private int    expires_in;
  /**
   * 授权范围
   */
  private String scope;
  /**
   * 失效时间(毫秒数)
   */
  private long   invalidTime;

  public boolean isValid() {
    return invalidTime > System.currentTimeMillis();
  }

  public void setExpires_in(int expires_in) {
    this.expires_in = expires_in;
    //提前一分钟失效
    this.invalidTime = System.currentTimeMillis() + (expires_in - 60) * 1000;
  }

  /**
   * 转成授权参数
   *
   * @return
   */
  public String toAuthorization() {
    return token_type + " " + access_token;
  }
}
