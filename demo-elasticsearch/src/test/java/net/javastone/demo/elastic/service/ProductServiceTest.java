package net.javastone.demo.elastic.service;

import net.javastone.demo.elastic.metadata.model.Category;
import net.javastone.demo.elastic.metadata.model.Product;
import net.javastone.demo.elastic.utils.Strings;
import net.javastone.demo.test.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.UUID;

/**
 * Created by sdliang on 2019/5/31.
 */
public class ProductServiceTest extends BaseTest {

  @Autowired
  private ProductService productService;

  @Test
  public void testSave() {
    Product productNew = newProduct();
    String id = productService.save(productNew);

    Product product = productService.get(id);

    Assert.assertNotNull(product);
    Assert.assertEquals("新增失败", id, product.getId());
  }

  @Test
  public void testQuery() {
    String keyword = "生活用品";
    Pageable pageable = PageRequest.of(0, 20);//, Sort.by("goodsCode"));
    Page<Product> productPage = productService.findByKeyword(keyword, pageable);

    Assert.assertTrue(productPage.getTotalElements() > 0);

    productPage.getContent().forEach(product -> {
      System.out.println(product.getId());
      System.out.println(product.getName());
    });
  }

  @Test
  public void testUpdate() {
    String id = "6b5ccaec625747ce825aa8904399c395";
    String newName = "冰红茶";
    Product product = productService.get(id);

    Assert.assertNotNull("修改失败", product);

    product.setName(newName);
    product.setSubTitle("冰红茶 清凉一夏 450ml 2元");
    productService.update(product);
    product = productService.get(id);

    Assert.assertEquals("修改失败", newName, product.getName());
  }

  @Test
  public void testDelete() {
    String id = "6b5ccaec625747ce825aa8904399c395";
    productService.deleteById(id);
    Product product = productService.get(id);

    Assert.assertNull("删除失败", product);
  }

  private Product newProduct() {
    Product product = new Product();

    product.setId(Strings.uuid());

    Category category = new Category();

    category.setId("71");
    category.setName("生活用品");

    product.setCategory(category);
    product.setName("康师傅 冰红茶 450ml");
    product.setBatchCode("BC10001");
    product.setBrandLogo("http://www.hilzg.com/images/201508/goods_img/8823_P_1440271315523.jpg");
    product.setBrandName("康师傅");
    product.setCountryLogo(
        "http://hbimg.b0.upaiyun.com/1f5ab4d974417bf4ea8810b455cbb0a7d9f342b456aab-1qlAdQ_fw658");
    product.setCountryMap(
        "http://hbimg.b0.upaiyun.com/1f5ab4d974417bf4ea8810b455cbb0a7d9f342b456aab-1qlAdQ_fw658");
    product.setCountryName("中国");
    product.setGoodsCode("G10001");
    product.setStatus(1);
    product.setSubTitle("康师傅 冰红茶 450ml 清凉一夏 618活动价 2元");

    return product;
  }
}
