package com.demo.spring.boot.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by BlueDream on 2017/11/6.
 */
@Component
@PropertySource("classpath:properties/app-config.properties")
@ConfigurationProperties
public class AppProperties {

    private String domain;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }
}
