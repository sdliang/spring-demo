package net.javastone.demo.elastic.service;

/**
 * Created by sdliang on 2019/5/31.
 */
public interface IndexService {

  /**
   * 创建
   *
   * @param name 索引名称或别名
   * @return
   */
  boolean create(String name);

  /**
   * 创建
   *
   * @param indexName
   * @param aliasName
   * @return
   */
  boolean create(String indexName, String aliasName);

  /**
   * 是否存在
   *
   * @param name
   * @return
   */
  boolean indexExists(String name);

  /**
   * 删除
   *
   * @param name
   * @return
   */
  boolean delete(String name);

  /**
   * 增加别名
   *
   * @param indexName
   * @param aliasName
   * @return
   */
  boolean addAlias(String indexName, String aliasName);

  /**
   * 删除别名
   *
   * @param indexName
   * @param aliasName
   * @return
   */
  boolean deleteAlias(String indexName, String aliasName);
}
