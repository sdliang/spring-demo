package net.javastone.demo.test;

import org.assertj.core.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by sdliang on 2019/5/31.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class BaseTest extends Assertions {

}
