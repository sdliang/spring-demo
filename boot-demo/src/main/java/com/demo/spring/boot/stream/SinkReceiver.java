package com.demo.spring.boot.stream;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

/**
 * Created by user-007 on 2017/11/27.
 */
@EnableBinding(value = {Sink.class})
public class SinkReceiver {

    @StreamListener(Sink.INPUT)
    public void receive(Object payload) {
        System.out.println("receive data:" + payload);
    }
}
