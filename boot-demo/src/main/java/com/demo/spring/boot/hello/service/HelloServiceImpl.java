package com.demo.spring.boot.hello.service;

import com.demo.spring.boot.hello.dao.HelloDao;
import com.demo.spring.boot.hello.pojo.Hello;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by BlueDream on 2017/11/6.
 */
@Service
public class HelloServiceImpl implements HelloService {

    private HelloDao helloDao;

    @Autowired(required = false)
    public void setHelloDao(HelloDao helloDao) {
        this.helloDao = helloDao;
    }

    @Override
    public String save(Hello hello) {
        helloDao.save(hello);
        return hello.getId();
    }

    @Override
    public void update(Hello hello) {
        if (helloDao.getOne(hello.getId()) != null) {
            helloDao.saveAndFlush(hello);
        }
        throw new RuntimeException("manual exception");
    }

    @Override
    public Hello delete(String id) {
        Hello hello = helloDao.getOne(id);
        if (hello == null) {
            throw new RuntimeException("找不到实体:" + id);
        }
        helloDao.deleteById(id);
        return hello;
    }

    @Override
    public Hello get(String id) {
        return helloDao.getOne(id);
    }

    @Override
    public List<Hello> queryByName(String name) {
        return helloDao.findByName(name);
    }
}
