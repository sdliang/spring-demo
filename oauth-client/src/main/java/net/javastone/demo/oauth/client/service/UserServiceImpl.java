package net.javastone.demo.oauth.client.service;

import net.javastone.demo.oauth.client.config.AppProperties;
import net.javastone.demo.oauth.client.model.AccessToken;
import net.javastone.demo.oauth.client.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private AccessTokenService accessTokenService;

  @Autowired
  private RestTemplate restTemplate;

  @Autowired
  private AppProperties appProperties;

  @Override
  public User get(String username) {
    return userData.get(username);
  }

  @Override
  public User remoteGet(String code) {
    //获取token
    AccessToken accessToken = accessTokenService.getAccessToken(code);
    if (accessToken == null || !accessToken.isValid()) {
      throw new RuntimeException("无效的授权服务token");
    }
    //请求资源服务器, 获取用户信息
    String username = remoteUser(accessToken);
    //转化用户信息
    return userData.get(username);
  }

  private String remoteUser(AccessToken accessToken) {

    ResponseEntity<String> responseEntity = restTemplate.exchange(URI.create(getUserCurrent()),
                                                                  HttpMethod.GET,
                                                                  getHttpEntity(accessToken), String.class);
    //结果处理
    if (responseEntity.getStatusCode() != HttpStatus.OK) {
      throw new RuntimeException("获取用户信息失败: " + responseEntity.getStatusCodeValue());
    }
    return responseEntity.getBody();
  }

  private HttpEntity getHttpEntity(AccessToken accessToken) {
    HttpHeaders headers = new HttpHeaders();
    headers.set("Authorization", accessToken.toAuthorization());
    return new HttpEntity(headers);
  }

  private String getUserCurrent() {
    return appProperties.getResourceUri() + "/user/current";
  }

  //用户数据
  private static final Map<String, User> userData = new HashMap<>();

  static {
    userData.put("user1", new User("1", "user1"));
    userData.put("user2", new User("2", "user2"));
    userData.put("user3", new User("3", "user3"));
    userData.put("sdliang", new User("4", "sdliang"));
  }
}
