package com.demo.spring.boot.hello.controller;

import com.demo.spring.boot.controller.BaseController;
import com.demo.spring.boot.properties.AppProperties;
import com.demo.spring.boot.hello.pojo.Hello;
import com.demo.spring.boot.hello.service.HelloService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by BlueDream on 2017/4/30.
 */
@RestController
@RequestMapping("/hello")
public class HelloBootController extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloBootController.class);

    static final String TEMPLATE = "Hello %s";
    static final String TEMPLATE_BODY = "Hello %s;\n性别 %s;\n年龄 %d";

    private AppProperties appProperties;

    private HelloService helloService;

    @Autowired(required = false)
    public void setHelloService(HelloService helloService) {
        this.helloService = helloService;
    }

    @Autowired(required = false)
    public void setAppProperties(AppProperties appProperties) {
        this.appProperties = appProperties;
    }

    @RequestMapping("/{id}")
    public Hello getById(@PathVariable("id") String id) {
        LOGGER.info("param is:" + id);
        return helloService.get(id);
    }


    @RequestMapping("/add")
    public String add(@RequestBody Hello hello) {
        return helloService.save(hello);
    }

    @RequestMapping("/update/{id}")
    public Hello update(@PathVariable("id") String id, @RequestBody Hello hello) {
        hello.setId(id);
        helloService.update(hello);
        return hello;
    }

    @RequestMapping("/list")
    public List<Hello> query(String name) {
        return helloService.queryByName(name);
    }

    @RequestMapping("/config/{key}")
    public String appValue(@PathVariable("key") String key) {
        return getConfigValue(appProperties, key);
    }

    @RequestMapping("/rest/random")
    public String restTpl() throws URISyntaxException {
        return restTemplate.getForObject("http://gturnquist-quoters.cfapps.io/api/random", String.class);
    }

    /**
     * 获取属性值
     *
     * @param appProperties
     * @param key
     * @return
     */
    static String getConfigValue(AppProperties appProperties, String key) {
        String value = "undefined";
        if ("domain".equalsIgnoreCase(key)) {
            value = appProperties.getDomain();
        }
        return value;
    }
}
