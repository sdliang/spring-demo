package com.demo.spring.boot;

import com.alibaba.druid.pool.DruidDataSource;
import com.demo.spring.boot.transaction.SineduTransactionAttributeSource;
import com.demo.spring.boot.transaction.SineduTransactionProps;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.interceptor.TransactionAttributeSource;

import javax.sql.DataSource;
import javax.xml.ws.WebServiceException;

/**
 * Created by BlueDream on 2017/4/30.
 */
@SpringBootApplication
@MapperScan(basePackages = {"com.demo.spring.boot..*.dao"})
@EnableTransactionManagement
//@PropertySource("classpath:application.yml")
//@ImportResource("classpath:transaction.xml")
public class Application {

  public static void main(String[] args) {
    new SpringApplicationBuilder(Application.class).web(true).run(args);
  }

  private Environment environment;

  @Autowired
  public void setEnvironment(Environment environment) {
    this.environment = environment;
  }

  /**
   * 数据源
   *
   * @return
   */
  @Bean(destroyMethod = "close")
  public DataSource dataSource() {
    DruidDataSource dataSource = new DruidDataSource();
    dataSource.setDriverClassName(environment.getProperty("datasource.driver"));
    dataSource.setUrl(environment.getProperty("datasource.url"));
    dataSource.setUsername(environment.getProperty("datasource.username"));
    dataSource.setPassword(environment.getProperty("datasource.password"));
    dataSource.setLoginTimeout(Integer.parseInt(environment.getProperty("datasource.timeout")));

    return dataSource;
  }

  @Bean
  public SineduTransactionProps transactionAttribute() {
    SineduTransactionProps transactionProps = new SineduTransactionProps();
    transactionProps.addRollbackFor(WebServiceException.class);
    return transactionProps;
  }

  /**
   * 自定义事务处理
   *
   * @param aspectSupport
   * @param transactionProps
   * @return
   */
  @Bean
  @ConditionalOnBean( {TransactionAspectSupport.class, SineduTransactionProps.class})
  public Object transation(TransactionAspectSupport aspectSupport,
                           SineduTransactionProps transactionProps) {
    if (aspectSupport == null || transactionProps == null) {
      return null;
    }
    TransactionAttributeSource attributeSource = aspectSupport.getTransactionAttributeSource();
    aspectSupport
        .setTransactionAttributeSource(new SineduTransactionAttributeSource(attributeSource,
            transactionProps));
    return null;
  }

}
