package net.javastone.demo.eureka.feign;

import net.javastone.demo.eureka.Org;
import net.javastone.demo.eureka.properties.FeignProperties;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by user-007 on 2017/11/8.
 */
@FeignClient(name = FeignProperties.API_ORG, fallback = OrgApiHystrix.class)
public interface OrgApi {

    @RequestMapping("/eureka/org/{name}")
    Org hiOrg(@PathVariable(value = "name") String name);
}
