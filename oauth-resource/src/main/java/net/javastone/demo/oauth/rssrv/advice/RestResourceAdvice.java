package net.javastone.demo.oauth.rssrv.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestResourceAdvice {

  private Logger logger = LoggerFactory.getLogger(RestResourceAdvice.class);

  @ExceptionHandler(RuntimeException.class)
  public ResponseEntity runtimeException(RuntimeException e) {
    logger.error(e.getMessage(), e);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity exception(Exception e) {
    logger.error(e.getMessage(), e);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
  }
}
