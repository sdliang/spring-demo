package net.javastone.demo.elastic.service;

import net.javastone.demo.elastic.metadata.model.Product;
import net.javastone.demo.elastic.metadata.repository.ProductRepo;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by sdliang on 2019/5/31.
 */
@Service
public class ProductServiceImpl implements ProductService {

  @Autowired
  private ProductRepo productRepo;

  @Transactional
  @Override
  public String save(Product product) {
    Product entity = productRepo.save(product);
    return entity.getId();
  }

  @Override
  public Product get(String id) {
    return productRepo.findById(id).orElse(null);
  }

  @Transactional
  @Override
  public void update(Product product) {
    productRepo.delete(product);
    productRepo.save(product);
  }

  @Transactional
  @Override
  public void deleteById(String id) {
    productRepo.deleteById(id);
  }

  @Override
  public Page<Product> findByKeyword(String keyword, Pageable pageable) {
    //名称/标题/品牌
    QueryBuilder builder = new MultiMatchQueryBuilder(keyword,
        "name",
        "subTitle",
        "brandName.keyword",
        "category.name.keyword");
    return productRepo.search(builder, pageable);
  }
}
