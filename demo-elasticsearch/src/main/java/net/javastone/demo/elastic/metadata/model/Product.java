package net.javastone.demo.elastic.metadata.model;

import lombok.Getter;
import lombok.Setter;
import net.javastone.demo.elastic.ESDemoConstants;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * Created by sdliang on 2019/5/31.
 */
@Getter
@Setter
@Document(indexName = ESDemoConstants.INDEX, type = ESDemoConstants.TYPE_DEFAULT, shards = 3,
          createIndex = false)
public class Product {

  @Id
  private String id;

  @Field(type = FieldType.Object)
  private Category category;

  @Field(type = FieldType.Text, analyzer = "ik_max_word")
  private String name;

  @Field(type = FieldType.Text, analyzer = "ik_max_word")
  private String subTitle;

  @Field(type = FieldType.Keyword)
  private String goodsCode;

  @Field(type = FieldType.Keyword)
  private String brandName;

  @Field(type = FieldType.Keyword, index = false)
  private String brandLogo;

  @Field(type = FieldType.Keyword)
  private String countryName;

  @Field(type = FieldType.Keyword, index = false)
  private String countryLogo;

  @Field(type = FieldType.Keyword, index = false)
  private String countryMap;

  @Field(type = FieldType.Keyword)
  private String batchCode;

  @Field(type = FieldType.Keyword)
  private int status;
}
